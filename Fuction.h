#ifndef COSC2131_LIFE_S3445846_FUCTION_H
#define COSC2131_LIFE_S3445846_FUCTION_H

#include <iostream>
#include <ncurses.h>

using namespace std;

namespace function {
    void copy(int from[20][20], int to[20][20]) {
        for (int i = 0; i < 20; ++i) {
            for (int j = 0; j < 20; ++j) {
                to[i][j] = from[i][j];
            }
        }
    }

    void life(int board[20][20]) {
        // Create a temporary array to store the board.
        int temp[20][20];
        copy(board, temp);

        for (int i = 1; i < 19; ++i) {
            for (int j = 1; j < 19; ++j) {
                // Count the neighborhoods at N, S, E and W of the cell.
                int count = 0;
                count = board[i-1][j] + board[i][j-1] + board[i+1][j] + board[i][j+1];

                // Cell will die.
                if (count < 2 || count > 3)
                    temp[i][j] = 0;
                // Cell wil stay the same.
                if (count == 2)
                    temp[i][j] = board[i][j];
                // Cell stays alive.
                if (count == 3)
                    temp[i][j] = 1;
            }
        }

        copy(temp, board);
    }

    void print(int array[20][20], int gen)
    {
        printw("Generation: %d", gen);
        printw("\n\n");
        for(int i = 1; i < 19; i++) {
            for(int j = 1; j < 19; j++) {
                if(array[i][j] == 1)
                    printw("+");
                else
                    printw(" ");

                refresh();
            }
            printw("\n");
        }
    }

    bool compare(int array1[20][20], int array2[20][20])
    {
        int count = 0;
        // Check is 2 arrays are exactly the same.
        for(int i = 0; i < 20; i++)
        {
            for(int j = 0; j < 20; j++)
            {
                if(array1[i][j]==array2[i][j])
                    count++;
            }
        }

        return count == 20 * 20;
    }

    void initializeBoard(int board[20][20]) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                board[i][j] = 0;
            }
        }

        for (int i = 1; i < 19; i++) {
            for (int j = 1; j < 19; j++) {
                board[i][j] = rand() % 2;
            }
        }
    }
}

#endif //COSC2131_LIFE_S3445846_FUCTION_H
