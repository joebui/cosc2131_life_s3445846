#include <ncurses.h>
#include <unistd.h>
#include "Fuction.h"

using namespace function;

int main() {
    // Initialize ncurses.
    initscr();

    int board[20][20];
    int backup[20][20];
    bool compareResult;
    initializeBoard(board);

    // Calculate number of simulations.
    int k = 0;

    do {
        copy(board, backup);
        // Print the board.
        print(board, k + 1);
        // Start the simulation.
        life(board);
        // Increase the simulation number.
        k++;

        // Pause the program for 1s.
        sleep(1);
        // Pause simulation if exceeding 100 times.
        if(k % 100 == 1 && k != 1) {
            printw("\n");
            int choice;
            do {
                // Ask if user wants to continue simulation.
                printw("Would you like to continue this simulation? (y/n): ");
                choice = getch();
            } while(choice != 'y' && choice != 'n');

            if(choice == 'n')
                break;
        }

        // Compare the 2 boards to check if simulation is finished.
        compareResult = compare(board, backup);
        if(!compareResult)
            clear();
        if(compareResult) {
            printw("The simulation is over. Press any key to exit.");
            refresh();
        }
    } while (!compareResult);

    // Ask for user input.
    getch();
    endwin();

    return 0;
}