Assignment 2 - Game of Life
Author: Bui Quang Dien (s3445846)

When opening the project, run the project and then click on "Terminal" at
the Tool Buttons. After that type $ ./bin/cosc2131_life_s3445846 to run the program.

References: http://code.runnable.com/UwQvQY99xW5AAAAQ/john-conway-s-game-of-life-for-c%2B%2B-nested-for-loops-and-2-dimensional-arrays
